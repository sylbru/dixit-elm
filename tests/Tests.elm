module Tests exposing (..)

import Browser.Navigation
import Expect
import Main exposing (..)
import Message
import Test exposing (..)
import Texts exposing (Language(..))


testParsePlayers : Test
testParsePlayers =
    describe "parsePlayers"
        [ test "Test 1" <|
            \_ ->
                [ "Sylvain", "0", "Guillaume", "0" ]
                    |> Message.parsePlayers
                    |> Expect.equal
                        [ { name = "Sylvain", score = 0 }
                        , { name = "Guillaume", score = 0 }
                        ]
        ]


testParseResults : Test
testParseResults =
    describe "parseResults"
        [ test "No results" <|
            \_ ->
                []
                    |> Message.parseResults
                    |> Expect.equal
                        []
        , test "One result" <|
            \_ ->
                [ "72", "Sylvain", "2", "Guillaume", "Martine" ]
                    |> Message.parseResults
                    |> Expect.equal
                        [ ( 72, "Sylvain", [ "Guillaume", "Martine" ] ) ]
        , test "Two results" <|
            \_ ->
                [ "72", "Sylvain", "2", "Guillaume", "Martine", "22", "Guillaume", "0" ]
                    |> Message.parseResults
                    |> Expect.equal
                        [ ( 72, "Sylvain", [ "Guillaume", "Martine" ] )
                        , ( 22, "Guillaume", [] )
                        ]
        , test "Three results" <|
            \_ ->
                [ "72", "Sylvain", "2", "Guillaume", "Martine", "22", "Guillaume", "0", "106", "Martine", "1", "Guillaume" ]
                    |> Message.parseResults
                    |> Expect.equal
                        [ ( 72, "Sylvain", [ "Guillaume", "Martine" ] )
                        , ( 22, "Guillaume", [] )
                        , ( 106, "Martine", [ "Guillaume" ] )
                        ]
        , test "Another three" <|
            \_ ->
                [ "72", "Sylvain", "2", "Guillaume", "Martine", "22", "Guillaume", "1", "Sylvain", "106", "Martine", "0" ]
                    |> Message.parseResults
                    |> Expect.equal
                        [ ( 72, "Sylvain", [ "Guillaume", "Martine" ] )
                        , ( 22, "Guillaume", [ "Sylvain" ] )
                        , ( 106, "Martine", [] )
                        ]
        , test "Wrong input" <|
            \_ ->
                [ "72", "Sylvain", "2", "Guillaume", "Martine", "22", "Guillaume", "0", "Sylvain", "106", "Martine", "0" ]
                    |> Message.parseResults
                    |> Expect.equal
                        [ ( 72, "Sylvain", [ "Guillaume", "Martine" ] )
                        , ( 22, "Guillaume", [] )
                        ]
        , test "Wrong input #2" <|
            \_ ->
                [ "sept", "Sylvain", "2", "Guillaume", "Martine", "22", "Guillaume", "0" ]
                    |> Message.parseResults
                    |> Expect.equal
                        []
        ]


testParseMessage : Test
testParseMessage =
    describe "parseMessage"
        [ test "Command without arguments" <|
            \_ ->
                "NEXTROUND"
                    |> Message.parse
                    |> Expect.equal ( "NEXTROUND", [] )
        , test "Command with one argument" <|
            \_ ->
                "TELLING Sylvain"
                    |> Message.parse
                    |> Expect.equal ( "TELLING", [ "Sylvain" ] )
        , test "Command with several arguments" <|
            \_ ->
                "CARDS 3 4 9 11 102"
                    |> Message.parse
                    |> Expect.equal ( "CARDS", [ "3", "4", "9", "11", "102" ] )
        , test "Leading whitespace" <|
            \_ ->
                "  CARDS 3 4 9 11 102"
                    |> Message.parse
                    |> Expect.equal ( "CARDS", [ "3", "4", "9", "11", "102" ] )
        , test "Trailing whitespace" <|
            \_ ->
                "CARDS 3 4 9 11 102 "
                    |> Message.parse
                    |> Expect.equal ( "CARDS", [ "3", "4", "9", "11", "102" ] )
        , test "Multiple consecutive spaces" <|
            \_ ->
                "CARDS 3 4  9 11 102 "
                    |> Message.parse
                    |> Expect.equal ( "CARDS", [ "3", "4", "9", "11", "102" ] )
        ]


testHome : Test
testHome =
    describe "Home"
        [ test "In-game messages are ignored when in home screen." <|
            \_ ->
                Home
                    { playerName = "Hey", gameName = "", joining = False, navigationKey = dummyKey }
                    English
                    |> update (ReceiveMessage "VOTING 2 4 Sylv1")
                    |> Expect.equal
                        ( Home
                            { playerName = "Hey", gameName = "", joining = False, navigationKey = dummyKey }
                            English
                        , Cmd.none
                        )
        ]


dummyKey : () -> Browser.Navigation.Key
dummyKey () =
    dummyKey ()


testVoting : Test
testVoting =
    describe "Voting"
        [ test "Card stays selected after receiving VOTING message" <|
            \_ ->
                Game
                    { players = [ { name = "Sylv1", score = 0 }, { name = "Guillaume II", score = 0 } ]
                    , me = "Sylv1"
                    , hand = [ 12, 42, 99, 1, 106 ] |> toHand |> withSelectedCard 42
                    , teller = Just "Guillaume II"
                    , phase = Guesser (Voting { cards = [ 2, 42 ], selected = Nothing } [ "Guillaume II", "Sylv1" ] (Just 42))
                    , navigationKey = dummyKey
                    }
                    French
                    |> update (ReceiveMessage "VOTING 2 4 Sylv1")
                    |> Expect.equal
                        ( Game
                            { players = [ { name = "Sylv1", score = 0 }, { name = "Guillaume II", score = 0 } ]
                            , me = "Sylv1"
                            , hand = [ 12, 42, 99, 1, 106 ] |> toHand |> withSelectedCard 42
                            , teller = Just "Guillaume II"
                            , phase = Guesser (Voting { cards = [ 2, 42 ], selected = Nothing } [ "Sylv1" ] (Just 42))
                            , navigationKey = dummyKey
                            }
                            French
                        , Cmd.none
                        )
        ]
