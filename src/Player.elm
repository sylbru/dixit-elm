module Player exposing (Player, PlayerName, PlayerStatus(..), new, withScore)


type alias Player =
    { name : PlayerName, score : Int }


type alias PlayerName =
    String


type PlayerStatus
    = Idle
    | ActionNeeded
    | Ready


new : String -> Player
new name =
    { name = name, score = 0 }


withScore : Int -> Player -> Player
withScore newScore player =
    { player | score = newScore }
