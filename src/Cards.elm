module Cards exposing (..)

import Player exposing (PlayerName)


type alias Card =
    Int


type alias CardResult =
    ( Card, PlayerName, List PlayerName )
