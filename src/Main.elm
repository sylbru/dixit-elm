port module Main exposing (..)

import Browser
import Browser.Dom as Dom
import Browser.Navigation
import Cards exposing (Card, CardResult)
import Html exposing (Html, div, h1, img, text)
import Html.Attributes as Attr exposing (class, classList, style)
import Html.Events as Events exposing (onClick)
import Json.Decode as Dec
import Json.Encode as Enc
import Message
import Player exposing (Player, PlayerName, PlayerStatus(..))
import Task
import Texts exposing (Language(..))
import Url exposing (Url)



---- MODEL ----


type Model
    = Home HomeModel Language WebsocketStatus
    | Game GameModel Language WebsocketStatus


type alias WebsocketStatus =
    Bool


type alias HomeModel =
    { playerName : PlayerName
    , gameName : String
    , joining : Bool
    , navigationKey : () -> Browser.Navigation.Key
    }


type alias GameModel =
    { players : List Player
    , me : PlayerName
    , hand : Hand
    , teller : Maybe PlayerName
    , phase : Phase
    , deck : ( Int, Int )
    , navigationKey : () -> Browser.Navigation.Key
    }


type Phase
    = NoPhase
    | Teller TellerPhase
    | Guesser GuesserPhase
    | Results (List CardResult) (List PlayerName)
    | GameFinished


type TellerPhase
    = Telling
    | WaitingForCards (List PlayerName)
    | WaitingForVotes (List Card) (List PlayerName)


type GuesserPhase
    = WaitingForTeller
    | Selecting (List PlayerName)
      -- Voting: the last value is the user’s card. It shouldn’t be a Maybe but since the server
      -- isn’t sending us the information, we take it from the hand where theoretically
      -- the card might be deselected after sending it to the server...
    | Voting Candidates (List PlayerName) (Maybe Card)


type alias Hand =
    { cards : List Card, selected : Maybe Card }


type alias Candidates =
    Hand


init : Enc.Value -> Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init flags url navigationKey =
    let
        testing =
            False

        ( savedPlayerName, lang ) =
            case Dec.decodeValue savedDataDecoder flags of
                Ok flagData ->
                    flagData

                Err _ ->
                    ( "", English )

        gameName =
            gameNameFromUrl url
    in
    if testing then
        ( testModel navigationKey, Cmd.none )

    else
        let
            playerName =
                String.trim savedPlayerName
        in
        case gameName of
            Just gameInUrl ->
                if playerName /= "" then
                    -- Game and player are memorized, we can join automatically
                    ( Home
                        { playerName = playerName
                        , gameName = gameInUrl
                        , joining = False
                        , navigationKey = \() -> navigationKey
                        }
                        lang
                        False
                    , joinGameAs gameInUrl playerName
                    )

                else
                    -- Game is memorized, but player is not, we can’t automatically join
                    ( Home
                        { playerName = ""
                        , gameName = gameInUrl
                        , joining = False
                        , navigationKey = \() -> navigationKey
                        }
                        lang
                        False
                    , focusPlayerNameInput
                    )

            Nothing ->
                -- Game is not memorized, we can’t automatically join
                ( Home
                    { playerName = playerName
                    , gameName = ""
                    , joining = False
                    , navigationKey = \() -> navigationKey
                    }
                    lang
                    False
                , focusPlayerNameInput
                )


focusPlayerNameInput : Cmd Msg
focusPlayerNameInput =
    Task.attempt (\_ -> NoOp) (Dom.focus playerNameInputId)


testModel : Browser.Navigation.Key -> Model
testModel navigationKey =
    let
        players =
            [ { name = "Sylv1", score = 0 }
            , { name = "Guillaume II", score = 42 }
            , { name = "Marie", score = 26 }
            , { name = "Mitrane", score = 7 }
            , { name = "Steph", score = 33 }
            ]
    in
    Game
        { players = players
        , me = "Sylv1"
        , hand = [ 12, 42, 99, 1, 106 ] |> toHand |> withSelectedCard 99
        , teller = Just "Guillaume II"
        , deck = ( 100, 160 )
        , navigationKey = \() -> navigationKey
        , phase = Results [ ( 1, "Martine", [ "Martine", "Sylv1" ] ), ( 11, "Guillaume II", [ "Sylv1" ] ), ( 106, "Sylv1", [] ) ] [ "Sylv1", "Guillaume II" ]

        --, phase = Guesser (Voting { cards = [ 1, 2, 3 ], selected = Nothing } [ "Sylv1" ] (Just 1))
        }
        English
        True


safeName : String -> String
safeName unsafeName =
    unsafeName |> String.filter (\char -> char /= ' ')



---- UPDATE ----


type Msg
    = NoOp
    | UpdatePlayerName String
    | UpdateGameName String
    | JoinGame String String
    | ReceiveMessage String
    | Tell Card
    | Select Card
    | UnSelect
    | Vote Card
    | UnVote
    | NextRound
    | UpdateWebsocketStatus Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdatePlayerName newName ->
            case model of
                Home homeModel lang socketStatus ->
                    if not homeModel.joining then
                        ( Home { homeModel | playerName = newName } lang socketStatus, Cmd.none )

                    else
                        -- If we are currently joining the game, don’t accept
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        UpdateGameName newName ->
            case model of
                Home homeModel lang socketStatus ->
                    if not homeModel.joining then
                        ( Home { homeModel | gameName = newName } lang socketStatus, Cmd.none )

                    else
                        -- If we are currently joining the game, don’t accept
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        JoinGame unsafePlayerName unsafeGameName ->
            let
                playerName =
                    safeName unsafePlayerName

                gameName =
                    safeName unsafeGameName
            in
            case model of
                Home homeModel lang socketStatus ->
                    ( Home { homeModel | playerName = playerName } lang socketStatus
                    , Cmd.batch
                        [ savePlayerName playerName
                        , saveGameName gameName
                        , joinGameAs gameName playerName
                        , Browser.Navigation.pushUrl (homeModel.navigationKey ()) gameName
                        ]
                    )

                _ ->
                    ( model, Cmd.none )

        Tell card ->
            ( case model of
                Game gameModel lang socketStatus ->
                    Game
                        { gameModel | hand = gameModel.hand |> withSelectedCard card }
                        lang
                        socketStatus

                _ ->
                    model
            , tell card
            )

        Select card ->
            ( case model of
                Game gameModel lang socketStatus ->
                    Game { gameModel | hand = gameModel.hand |> withSelectedCard card } lang socketStatus

                _ ->
                    model
            , select card
            )

        UnSelect ->
            ( case model of
                Game gameModel lang socketStatus ->
                    Game { gameModel | hand = gameModel.hand |> withUnselectCard } lang socketStatus

                _ ->
                    model
            , unselect
            )

        Vote card ->
            ( case model of
                Game gameModel lang socketStatus ->
                    case gameModel.phase of
                        Guesser (Voting candidates remainingPlayers theirCard) ->
                            Game
                                { gameModel
                                    | phase = Guesser (Voting { candidates | selected = Just card } remainingPlayers theirCard)
                                }
                                lang
                                socketStatus

                        _ ->
                            model

                _ ->
                    model
            , vote card
            )

        UnVote ->
            ( case model of
                Game gameModel lang socketStatus ->
                    case gameModel.phase of
                        Guesser (Voting candidates remainingPlayers theirCard) ->
                            Game
                                { gameModel
                                    | phase = Guesser (Voting { candidates | selected = Nothing } remainingPlayers theirCard)
                                }
                                lang
                                socketStatus

                        _ ->
                            model

                _ ->
                    model
            , unvote
            )

        NextRound ->
            ( model, nextRound )

        ReceiveMessage message ->
            case handleMessage message model of
                Ok newModel ->
                    ( newModel, Cmd.none )

                Err error ->
                    ( model, alert error )

        UpdateWebsocketStatus isOpen ->
            ( case model of
                Home homeModel lang websocketStatus ->
                    Home homeModel lang isOpen

                Game gameModel lang websocketStatus ->
                    Game gameModel lang isOpen
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    { title = "Dixit\u{202F}!"
    , body = [ viewHtml model ]
    }


viewHtml : Model -> Html Msg
viewHtml model =
    case model of
        Home homeModel lang socketStatus ->
            let
                playerName =
                    homeModel.playerName

                gameName =
                    homeModel.gameName
            in
            div [ class "home", classList [ ( "socket-closed", not socketStatus ) ] ]
                [ div [ class "home__form" ]
                    [ Html.input
                        [ class "home__player-name"
                        , Attr.value playerName
                        , Attr.id playerNameInputId
                        , Events.onInput UpdatePlayerName
                        , Attr.required True
                        , Attr.placeholder (Texts.get lang Texts.YourName)
                        , onEnter (JoinGame playerName gameName)
                        ]
                        []
                    , Html.input
                        [ class "home__game-name"
                        , Attr.value gameName
                        , Events.onInput UpdateGameName
                        , Attr.placeholder (Texts.get lang Texts.GameName)
                        , onEnter (JoinGame playerName gameName)
                        ]
                        []
                    , Html.button
                        [ class "home__join-game"
                        , Events.onClick (JoinGame playerName gameName)
                        ]
                        [ Texts.get lang Texts.Join |> text ]
                    ]
                ]

        Game gameModel lang socketStatus ->
            viewGame lang gameModel socketStatus


playerNameInputId : String
playerNameInputId =
    "player_name"


onEnter : Msg -> Html.Attribute Msg
onEnter msg =
    -- Thank you github.com/evancz/elm-todomvc
    let
        isEnter code =
            if code == 13 then
                Dec.succeed msg

            else
                Dec.fail "not Enter"
    in
    Events.on "keydown" (Dec.andThen isEnter Events.keyCode)


viewGame : Language -> GameModel -> WebsocketStatus -> Html Msg
viewGame lang gameModel socketStatus =
    div [ class "game", classList [ ( "socket-closed", not socketStatus ) ] ]
        [ viewDeck gameModel.deck (List.length gameModel.players)
        , div [ class "players" ] (viewPlayers gameModel)
        , viewStatus lang gameModel.phase gameModel.me
        , div [ class "cards" ]
            (case gameModel.phase of
                Teller (WaitingForVotes cards _) ->
                    viewCandidatesAsTeller cards

                Guesser (Voting candidates _ theirCard) ->
                    viewCandidatesAsGuesser candidates theirCard

                Results results _ ->
                    viewResults lang results gameModel.teller

                _ ->
                    viewHand gameModel
            )
        ]


viewCandidatesAsTeller : List Card -> List (Html Msg)
viewCandidatesAsTeller cards =
    List.map (viewCard Nothing Nothing) cards


viewCandidatesAsGuesser : Candidates -> Maybe Card -> List (Html Msg)
viewCandidatesAsGuesser candidates theirCard =
    List.map
        (\card ->
            if theirCard /= Just card then
                viewCard (Just Vote) candidates.selected card

            else
                viewDisabledCard card
        )
        candidates.cards


viewResults : Language -> List CardResult -> Maybe PlayerName -> List (Html Msg)
viewResults lang results maybeTeller =
    List.map (viewResult maybeTeller) results


viewResult : Maybe PlayerName -> CardResult -> Html Msg
viewResult maybeTeller ( card, playedBy, votedForBy ) =
    div [ class "card", class "card--result", Attr.classList [ ( "card--teller", maybeTeller == Just playedBy ) ] ]
        [ div [ class "card__playedby" ] [ text playedBy ]
        , cardImg card
        , div [ class "card__votedforby" ] (List.map (\player -> div [ class "card__voter" ] [ text player ]) votedForBy)
        ]


viewStatus : Language -> Phase -> PlayerName -> Html Msg
viewStatus lang phase me =
    let
        statusDiv : Maybe String -> Texts.Code -> List (Html Msg) -> Html Msg
        statusDiv maybeModifier textCode additionalElements =
            let
                classes =
                    case maybeModifier of
                        Just modifier ->
                            [ class "status", class ("status--" ++ modifier) ]

                        Nothing ->
                            [ class "status" ]
            in
            div classes ((Texts.get lang textCode |> text) :: additionalElements)
    in
    case phase of
        NoPhase ->
            statusDiv Nothing Texts.WaitingForServer []

        Teller Telling ->
            statusDiv (Just "telling") Texts.TellerTelling []

        Teller (WaitingForCards remainingPlayers) ->
            statusDiv (Just "waitingforcards") Texts.TellerWaitingForCards []

        Teller (WaitingForVotes cards remainingPlayers) ->
            statusDiv (Just "waitingforvotes") Texts.TellerWaitingForVotes []

        Guesser WaitingForTeller ->
            statusDiv (Just "waitingforteller") Texts.GuesserWaitingForTeller []

        Guesser (Selecting remainingPlayers) ->
            statusDiv (Just "selecting") Texts.GuesserSelecting []

        Guesser (Voting candidates remainingPlayers _) ->
            statusDiv (Just "voting") Texts.GuesserVoting []

        Results results remainingPlayers ->
            let
                isReady =
                    not (List.member me remainingPlayers)
            in
            statusDiv (Just "results") Texts.Results [ viewNextRoundButton lang isReady ]

        GameFinished ->
            statusDiv (Just "gamefinished") Texts.GameFinished []


viewNextRoundButton : Language -> Bool -> Html Msg
viewNextRoundButton lang isReady =
    let
        label =
            if isReady then
                Texts.get lang Texts.WaitingForNextRound

            else
                Texts.get lang Texts.NextRound
    in
    Html.button
        [ class "next-round", Events.onClick NextRound, Attr.disabled isReady ]
        [ label |> text ]


viewPlayers : GameModel -> List (Html Msg)
viewPlayers gameModel =
    gameModel.players
        |> List.map
            (\player ->
                div [ Attr.class "player", Attr.classList [ ( "player--teller", gameModel.teller == Just player.name ) ] ]
                    [ div [ class "player__score" ] [ text (String.fromInt player.score) ]
                    , div [ class "player__name" ] [ text player.name ]
                    , div
                        [ class "player__status" ]
                        [ text
                            (playerStatusFromPhase player.name gameModel.teller gameModel.phase
                                |> playerStatusToString
                            )
                        ]
                    ]
            )


playerStatusToString : PlayerStatus -> String
playerStatusToString playerStatus =
    case playerStatus of
        Ready ->
            "✓"

        ActionNeeded ->
            "…"

        Idle ->
            ""


playerStatusFromPhase : PlayerName -> Maybe PlayerName -> Phase -> PlayerStatus
playerStatusFromPhase playerName maybeTellerName phase =
    let
        playerStatusFromRemainingPlayers : List PlayerName -> PlayerStatus
        playerStatusFromRemainingPlayers playersWithActionNeeded =
            case maybeTellerName of
                Just tellerName ->
                    if List.member playerName playersWithActionNeeded then
                        ActionNeeded

                    else if playerName == tellerName then
                        Idle

                    else
                        Ready

                Nothing ->
                    Idle

        playerStatusFromRemainingPlayersIncludingTeller : List PlayerName -> PlayerStatus
        playerStatusFromRemainingPlayersIncludingTeller playersWithActionNeeded =
            if List.member playerName playersWithActionNeeded then
                ActionNeeded

            else
                Ready

        playerStatusFromTeller : PlayerStatus
        playerStatusFromTeller =
            case maybeTellerName of
                Just tellerName ->
                    if playerName == tellerName then
                        ActionNeeded

                    else
                        Idle

                Nothing ->
                    Idle
    in
    case phase of
        NoPhase ->
            Idle

        Teller Telling ->
            playerStatusFromTeller

        Teller (WaitingForCards remainingPlayers) ->
            playerStatusFromRemainingPlayers remainingPlayers

        Teller (WaitingForVotes _ remainingPlayers) ->
            playerStatusFromRemainingPlayers remainingPlayers

        Guesser WaitingForTeller ->
            playerStatusFromTeller

        Guesser (Selecting remainingPlayers) ->
            playerStatusFromRemainingPlayers remainingPlayers

        Guesser (Voting _ remainingPlayers _) ->
            playerStatusFromRemainingPlayers remainingPlayers

        Results _ remainingPlayers ->
            playerStatusFromRemainingPlayersIncludingTeller remainingPlayers

        GameFinished ->
            Idle


viewHand : GameModel -> List (Html Msg)
viewHand gameModel =
    let
        maybeCardMsg =
            case gameModel.phase of
                Teller Telling ->
                    Just Tell

                Guesser (Selecting _) ->
                    Just Select

                _ ->
                    Nothing

        selectedCard =
            case gameModel.phase of
                Teller (WaitingForCards _) ->
                    gameModel.hand.selected

                Guesser (Selecting _) ->
                    gameModel.hand.selected

                -- This function is not used in the Voting phase, we’re done here
                _ ->
                    Nothing
    in
    List.map (viewCard maybeCardMsg selectedCard) gameModel.hand.cards


viewCard : Maybe (Card -> Msg) -> Maybe Card -> Card -> Html Msg
viewCard maybeCardMsg selectedCard card =
    let
        isSelected =
            selectedCard == Just card

        classes =
            [ class "card", classList [ ( "card--selected", isSelected ) ] ]

        attributes =
            case maybeCardMsg of
                Just cardMsg ->
                    if isSelected then
                        Events.onClick (deselectMessage (cardMsg card)) :: classes

                    else
                        Events.onClick (cardMsg card) :: classes

                Nothing ->
                    classes
    in
    div attributes [ cardImg card ]


viewDisabledCard : Card -> Html Msg
viewDisabledCard card =
    div [ class "card", class "card--disabled" ] [ cardImg card ]


viewDeck : ( Int, Int ) -> Int -> Html Msg
viewDeck ( remaining, total ) playerCount =
    let
        cardsPerPlayer =
            6

        initialTotal =
            total - playerCount * cardsPerPlayer

        progress =
            (toFloat (initialTotal - remaining) / toFloat initialTotal) * 100
    in
    div
        [ class "progress" ]
        [ div
            [ class "progress__bar"
            , style "width" (String.fromInt (round progress) ++ "%")
            , Attr.title ("Reste " ++ String.fromInt remaining ++ " sur " ++ String.fromInt total)
            ]
            []
        ]


deselectMessage : Msg -> Msg
deselectMessage msg =
    case msg of
        Select card ->
            UnSelect

        Vote card ->
            UnVote

        _ ->
            msg


cardImg : Card -> Html Msg
cardImg card =
    let
        imagePath =
            "cards/" ++ String.fromInt card ++ ".jpg"
    in
    Html.img [ Attr.src imagePath ] []



---- NETWORKING ----


joinGameAs : String -> String -> Cmd Msg
joinGameAs gameName playerName =
    if not (gameName |> String.trim |> String.isEmpty) then
        sendCommand "JOINGAME" [ gameName, playerName ]

    else if not (playerName |> String.trim |> String.isEmpty) then
        sendCommand "NAME" [ playerName ]

    else
        Cmd.none


tell : Card -> Cmd Msg
tell card =
    sendCommand "TELL" [ String.fromInt card ]


select : Card -> Cmd Msg
select card =
    sendCommand "SELECT" [ String.fromInt card ]


unselect : Cmd Msg
unselect =
    sendCommand "SELECT" []


vote : Card -> Cmd Msg
vote card =
    sendCommand "VOTE" [ String.fromInt card ]


unvote : Cmd Msg
unvote =
    sendCommand "VOTE" []


nextRound : Cmd Msg
nextRound =
    sendCommand "NEXTROUND" []


sendCommand : String -> List String -> Cmd Msg
sendCommand command arguments =
    command
        :: arguments
        |> String.join " "
        |> sendToSocket


handleMessage : String -> Model -> Result String Model
handleMessage message model =
    case Message.parse message of
        ( "PLAYERS", newPlayers ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game { gameModel | players = Message.parsePlayers newPlayers } lang socketStatus

                    Home homeModel lang socketStatus ->
                        Game
                            { players = Message.parsePlayers newPlayers
                            , me = homeModel.playerName
                            , hand = { cards = [], selected = Nothing }
                            , teller = Nothing
                            , phase = NoPhase
                            , deck = ( 1, 1 )
                            , navigationKey = homeModel.navigationKey
                            }
                            lang
                            socketStatus
                )

        ( "CARDS", newCards ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game { gameModel | hand = newCards |> Message.parseCards |> toHand } lang socketStatus

                    _ ->
                        model
                )

        ( "TELLING", playerName :: _ ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        let
                            gamePhase =
                                if playerName == gameModel.me then
                                    Teller Telling

                                else
                                    Guesser WaitingForTeller
                        in
                        Game
                            { gameModel
                                | teller = Just playerName
                                , phase = gamePhase
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "TELLER", tellerName :: [] ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game { gameModel | teller = Just tellerName } lang socketStatus

                    _ ->
                        model
                )

        ( "SELECTING", players ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | phase =
                                    if isTeller gameModel then
                                        Teller (WaitingForCards players)

                                    else
                                        Guesser (Selecting players)
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "CANDIDATES", cardsArguments ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        let
                            candidates =
                                cardsArguments |> List.filterMap String.toInt
                        in
                        case gameModel.phase of
                            -- useless if CANDIDATES is always sent before VOTING
                            Teller (WaitingForVotes _ remainingPlayers) ->
                                Game { gameModel | phase = Teller (WaitingForVotes candidates remainingPlayers) } lang socketStatus

                            Teller _ ->
                                Game { gameModel | phase = Teller (WaitingForVotes candidates []) } lang socketStatus

                            -- useless if CANDIDATES is always sent before VOTING
                            Guesser (Voting votingHand remainingPlayers theirCard) ->
                                Game { gameModel | phase = Guesser (Voting { votingHand | cards = candidates } remainingPlayers theirCard) } lang socketStatus

                            Guesser _ ->
                                Game { gameModel | phase = Guesser (Voting { cards = candidates, selected = Nothing } [] gameModel.hand.selected) } lang socketStatus

                            _ ->
                                Game
                                    { gameModel
                                        | phase =
                                            case gameModel.teller of
                                                Just tellerName ->
                                                    if tellerName == gameModel.me then
                                                        Teller (WaitingForVotes candidates [])

                                                    else
                                                        Guesser (Voting { cards = candidates, selected = Nothing } [] gameModel.hand.selected)

                                                Nothing ->
                                                    NoPhase
                                    }
                                    lang
                                    socketStatus

                    _ ->
                        model
                )

        ( "VOTING", newRemainingPlayers ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        case gameModel.phase of
                            Teller (WaitingForVotes candidates _) ->
                                Game { gameModel | phase = Teller (WaitingForVotes candidates newRemainingPlayers) } lang socketStatus

                            -- useless if CANDIDATES is always sent before VOTING
                            -- (CANDIDATES should have triggered the phase change)
                            Teller _ ->
                                Game { gameModel | phase = Teller (WaitingForVotes [] newRemainingPlayers) } lang socketStatus

                            Guesser (Voting candidates _ theirCard) ->
                                Game { gameModel | phase = Guesser (Voting candidates newRemainingPlayers theirCard) } lang socketStatus

                            -- useless if CANDIDATES is always sent before VOTING
                            -- (CANDIDATES should have triggered the phase change)
                            Guesser _ ->
                                Game { gameModel | phase = Guesser (Voting { cards = [], selected = Nothing } newRemainingPlayers gameModel.hand.selected) } lang socketStatus

                            _ ->
                                model

                    _ ->
                        model
                )

        ( "RESULTS", arguments ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | phase =
                                    Results (Message.parseResults arguments) []
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "WAITING", remainingPlayers ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | phase =
                                    case gameModel.phase of
                                        Results results _ ->
                                            Results results remainingPlayers

                                        _ ->
                                            gameModel.phase
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "GAMEFINISHED", arguments ) ->
            case model of
                Game gameModel lang socketStatus ->
                    Ok
                        (Game
                            { gameModel
                                | phase = GameFinished
                                , players = Message.parsePlayers arguments
                                , hand = gameModel.hand |> withUnselectCard
                                , teller = Nothing
                            }
                            lang
                            socketStatus
                        )

                Home _ lang socketStatus ->
                    Err (Texts.get lang Texts.ErrorGameFinished)

        ( "TOLD", card ) ->
            let
                maybeCard =
                    card
                        |> List.head
                        |> Maybe.andThen String.toInt
            in
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        -- set the selected card regardless of whether or not the player
                        -- is the teller, we trust the server (plus the correct phase might be set
                        -- just after with a SELECTING message)
                        Game
                            { gameModel
                                | hand =
                                    gameModel.hand |> withMaybeSelectedCard maybeCard
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "SELECTED", card ) ->
            let
                maybeCard =
                    card
                        |> List.head
                        |> Maybe.andThen String.toInt
            in
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | hand =
                                    gameModel.hand |> withMaybeSelectedCard maybeCard
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "VOTED", card ) ->
            let
                maybeCard =
                    card
                        |> List.head
                        |> Maybe.andThen String.toInt
            in
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | hand =
                                    gameModel.hand |> withMaybeSelectedCard maybeCard
                                , phase = Guesser (Voting { cards = [], selected = maybeCard } [] gameModel.hand.selected)
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "DECK", cardsRemainingString :: totalCardsString :: [] ) ->
            Ok
                (case model of
                    Game gameModel lang socketStatus ->
                        Game
                            { gameModel
                                | deck =
                                    ( String.toInt cardsRemainingString |> Maybe.withDefault 1
                                    , String.toInt totalCardsString |> Maybe.withDefault 1
                                    )
                            }
                            lang
                            socketStatus

                    _ ->
                        model
                )

        ( "ERROR", arguments ) ->
            Err (arguments |> String.join " ")

        ( unknownCommand, arguments ) ->
            Err
                ("Unknown or invalid command: "
                    ++ (unknownCommand :: arguments |> String.join " ")
                )


isTeller : GameModel -> Bool
isTeller gameModel =
    case gameModel.teller of
        Just teller ->
            gameModel.me == teller

        _ ->
            False


toHand : List Card -> Hand
toHand cards =
    { cards = cards, selected = Nothing }


withMaybeSelectedCard : Maybe Card -> Hand -> Hand
withMaybeSelectedCard maybeCard hand =
    { hand | selected = maybeCard }


withSelectedCard : Card -> Hand -> Hand
withSelectedCard card hand =
    { hand | selected = Just card }


withUnselectCard : Hand -> Hand
withUnselectCard hand =
    { hand | selected = Nothing }



---- PORTS ----


port sendToSocket : String -> Cmd msg


port receiveFromSocket : (String -> msg) -> Sub msg


port savePlayerName : String -> Cmd msg


port saveGameName : String -> Cmd msg


port alert : String -> Cmd msg


port updateWebsocketStatus : (Bool -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ receiveFromSocket ReceiveMessage
        , updateWebsocketStatus UpdateWebsocketStatus
        ]


{-| When a link is clicked. Here it might be used for the logout link
-}
onUrlRequest : Browser.UrlRequest -> Msg
onUrlRequest request =
    NoOp


onUrlChange : Url -> Msg
onUrlChange url =
    NoOp


savedDataDecoder : Dec.Decoder ( String, Language )
savedDataDecoder =
    let
        savedPlayerNameDecoder : Dec.Decoder String
        savedPlayerNameDecoder =
            Dec.field "savedPlayerName" Dec.string

        languageDecoder : Dec.Decoder Language
        languageDecoder =
            let
                codeToLanguage : String -> Language
                codeToLanguage code =
                    case code of
                        "fr" ->
                            French

                        "en" ->
                            English

                        _ ->
                            English
            in
            Dec.field "language" Dec.string
                |> Dec.map codeToLanguage
    in
    Dec.map2 (\p l -> ( p, l )) savedPlayerNameDecoder languageDecoder


gameNameFromUrl : Url -> Maybe String
gameNameFromUrl url =
    (case String.uncons url.path of
        Nothing ->
            url.fragment

        Just ( '/', "" ) ->
            url.fragment

        Just ( _, path ) ->
            Just path
    )
        |> Maybe.andThen Url.percentDecode



---- PROGRAM ----


main : Program Enc.Value Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = onUrlRequest
        , onUrlChange = onUrlChange
        }
