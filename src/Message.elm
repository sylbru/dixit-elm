module Message exposing (..)

import Cards exposing (Card, CardResult)
import Player exposing (Player)


parse : String -> ( String, List String )
parse message =
    let
        words : List String
        words =
            message
                |> String.trim
                |> String.split " "
                |> List.filter (not << String.isEmpty)
    in
    case words of
        command :: arguments ->
            ( command, arguments )

        _ ->
            ( "", [] )


parsePlayers : List String -> List Player
parsePlayers playersData =
    case playersData of
        name :: score :: rest ->
            (Player.new name
                |> Player.withScore (score |> String.toInt |> Maybe.withDefault 0)
            )
                :: parsePlayers rest

        _ ->
            []


parseCards : List String -> List Card
parseCards cardsData =
    cardsData
        |> List.filterMap String.toInt


parseResults : List String -> List CardResult
parseResults arguments =
    case arguments of
        cardString :: playedBy :: voteCountString :: rest ->
            -- We have enough arguments
            let
                maybeCard =
                    String.toInt cardString

                maybeVoteCount =
                    String.toInt voteCountString
            in
            Maybe.map2
                -- Assume card and vote count are valid ints
                (\card voteCount ->
                    if List.length rest >= voteCount then
                        -- rest contains the names of voters for this card,
                        -- and the rest of the argument list. We take the first arguments,
                        -- then parse the rest of the list.
                        ( card, playedBy, List.take voteCount rest )
                            :: parseResults (List.drop voteCount rest)

                    else
                        []
                )
                maybeCard
                maybeVoteCount
                |> Maybe.withDefault []

        _ ->
            []
