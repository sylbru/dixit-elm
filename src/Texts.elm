module Texts exposing (Code(..), Language(..), get)


type Language
    = French
    | English


type Code
    = Join
    | YourName
    | GameName
    | WaitingForServer
    | TellerTelling
    | TellerWaitingForCards
    | TellerWaitingForVotes
    | GuesserWaitingForTeller
    | GuesserSelecting
    | GuesserVoting
    | Results
    | NextRound
    | WaitingForNextRound
    | GameFinished
    | ErrorGameFinished


get : Language -> Code -> String
get language =
    getTexts
        >> (case language of
                French ->
                    .fr

                English ->
                    .en
           )


getTexts : Code -> { fr : String, en : String }
getTexts code =
    case code of
        YourName ->
            { fr = "Votre nom"
            , en = "Your name"
            }

        GameName ->
            { fr = "Partie à rejoindre"
            , en = "Game to join"
            }

        Join ->
            { fr = "Rejoindre"
            , en = "Join game"
            }

        NextRound ->
            { fr = "Passer au tour suivant"
            , en = "Move on to next round"
            }

        WaitingForNextRound ->
            { fr = "En attente des autres joueurs…"
            , en = "Waiting for other players…"
            }

        WaitingForServer ->
            { fr = "En attente…"
            , en = "Waiting for server…"
            }

        TellerTelling ->
            { fr = "Choisissez une carte et donnez un indice."
            , en = "Choose a card and tell a clue."
            }

        TellerWaitingForCards ->
            { fr = "Les autres joueurs choisissent une carte correspondant à votre indice."
            , en = "The other players are selecting a card."
            }

        TellerWaitingForVotes ->
            { fr = "Les autres joueurs sont en train de voter."
            , en = "The other players are voting."
            }

        GuesserWaitingForTeller ->
            { fr = "Le conteur est en train de choisir une carte et de donner un indice."
            , en = "The teller is choosing a card and telling a clue."
            }

        GuesserSelecting ->
            { fr = "Sélectionnez une carte correspondant à l’indice."
            , en = "Select a card that matches the clue."
            }

        GuesserVoting ->
            { fr = "Votez pour la carte que vous pensez être celle du conteur."
            , en = "Vote for whatever card you think is the teller’s card."
            }

        Results ->
            { fr = "Le tour est fini. Voici les résultats."
            , en = "The round is over. Check out the results."
            }

        GameFinished ->
            { fr = "Cette partie est terminée. Bravo à tous\u{202F}!"
            , en = "This game is over. Well done, everyone!"
            }

        ErrorGameFinished ->
            { fr = "Cette partie est terminée."
            , en = "This game is over."
            }
