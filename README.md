# Dixit

A Dixit client written in Elm, designed to work with the server written in Elixir available at https://github.com/guillaumebrunerie/dixit-elixir.
